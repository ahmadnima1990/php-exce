<?php
function isOdd($num) {
    if ($num % 2 == 0) {
        throw new Exception("{$num} is not odd number");
    }
    return $num;
}

try {
    echo isOdd(5) . "\n";
    echo isOdd(0) . "\n";
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}

// Continue execution
echo "Hello World\n";
?>